<?php


Class Twitterbot {

	private $apiKey;
	private $apiSecret;
	private $accessToken;
	private $accessTokenSecret;
	protected $tweetLimit = 1;
	protected $totalTweets = 0;

	//set to 'verbose' to see more log messages	
	public $logType = "";
	public $testMode = false;

	public function __construct($apiKey, $apiSecret, $accessToken, $accessTokenSecret) {

		$this->apiKey($apiKey);
		$this->apiSecret($apiSecret);
		$this->accessToken($accessToken);
		$this->accessTokenSecret($accessTokenSecret);

		$this->connection = new \TwitterOAuth(
						$this->apiKey(), 
						$this->apiSecret(), 
						$this->accessToken(),
						$this->accessTokenSecret()
					);
		$this->log($this->connection);
		$this->user = $this->connection->get('account/verify_credentials');
		$this->log($this->user);
	}

	protected function log($value) {
		
		if(($this->logType == "verbose" && !is_string($value)) || is_string($value)) {
			//error_log(print_r($value, true));
			print_r($value);
			echo "\r\n";

		}
	}

	public function tweetLimit($num=null) {
		if(is_int($num)) {
			$this->tweetLimit = $num;
		}
		return $this->tweetLimit;
	}

	public function tweet($text, $options=array()) {

		$options["status"] = $text;

		$this->log("Tweet: ".$text);

		if(!$this->testMode) {
			$responce = $this->connection->post('statuses/update', $options);
			$this->totalTweets++;
			return $responce;
		} else {
			return false;
		}
	}

	public function retweet($id) {

		if(is_object($id)) {
			$tweet = $id;
			$id = $tweet->id_str;
		}

		if($this->tweetLimit > $this->totalTweets) {
			$result = $this->connection->post("statuses/retweet/".$id);
			if(isset($result->errors)) {
				$this->log("Error: tweet not retweeted for reasons");
			} else {
				$this->totalTweets++;
				$this->log($result);
				$this->log("Retweet: \r\n\t ".$result->text);
			}
		} else {
			$this->log("NOT RETWEETED (Over tweetLimit): ".($tweet ? $tweet->text : $id));
		}

		return !empty($result) ? $result : false;

	}

	public function search($search, $options=array()) {
		
		if(empty($search)) {
			$this->log("You must search for something");
			return false;
		}

		$options['q'] = $search;
		$options['count'] = !empty($options["count"]) ? $options['count'] : 20; 
		$options['result_type'] = !empty($options["result_type"]) ? $options['result_type'] : 'recent'; 

		$result = $this->connection->get('search/tweets', $options);
		return $result;
	}

	public function getUser($options) {
		if(empty($options["screen_name"]) &&  empty($options["user_id"])) {
			$this->log("Error: screen_name or user_id is required to get user info");
			return false;
		}
		$responce = $this->connection->get('users/show', $options);

		return $responce;
	}

	public function apiKey($value="") {
		if(!empty($value)) {
			$this->apiKey = $value;
			$this->log("Set apiKey to: ".$this->apiKey);
		}
		return $this->apiKey;
	}

	public function apiSecret($value="") {
		if(!empty($value)) {
			$this->apiSecret = $value;
			$this->log("Set apiSecret to: ".$this->apiSecret);
		}
		return $this->apiSecret;
	}

	public function accessToken($value="") {
		if(!empty($value)) {
			$this->accessToken = $value;
			$this->log("Set accessToken to: ".$this->accessToken);
		}
		return $this->accessToken;
	}

	public function accessTokenSecret($value="") {
		if(!empty($value)) {
			$this->accessTokenSecret = $value;
			$this->log("Set accessTokenSecret to: ".$this->accessTokenSecret);
		}
		return $this->accessTokenSecret;
	}
}

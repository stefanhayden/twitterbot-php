<?php


class retweetBot extends Twitterbot {

	public function __construct($apiKey, $apiSecret, $accessToken, $accessTokenSecret) {
		parent::__construct($apiKey, $apiSecret, $accessToken, $accessTokenSecret);

		$user_tweets = $this->connection->get('statuses/user_timeline', array('screen_name' => $this->user->screen_name, 'count' => 20, 'include_rts' => 1));
		foreach($user_tweets as $tweet){
			$this->tweets_retweeted[] = $tweet->id_str;
			if($tweet->retweeted_status) {
				$this->tweets_retweeted[] = $tweet->retweeted_status->id_str;
				$this->users_retweeted[] = $tweet->retweeted_status->user->id_str;
			}
		}
		
	}

	public function searchAndRetweet($searches) {
		foreach($searches as $search) {
			$search_results = $this->search($search);

			foreach($search_results->statuses as $result) {

				$t = strtolower($result->text);

				if($this->tweetLimit == $this->totalTweets) {
					$this->log("Skip (tweet limit): ". $result->text);
					break;
				}
				if(!empty($this->tweets_retweeted) && in_array($result->id_str, $this->tweets_retweeted)) {
					$this->log("Skip (already retweeted tweet): ". $result->text);
					continue;
				}
				if(!empty($this->users_retweeted) && in_array($result->user->id_str, $this->users_retweeted)) {
					$this->log("Skip (already retweeted user): ". $result->text);
					continue;
				}
				if(strpos($t,'rt') === 0) {
					$this->log("Skip (is a retweet): ". $result->text);
					continue;
				}
				if(strpos($t,'@') === 0) {
					$this->log("Skip (@reply): ". $result->text);
					continue;
					
				}

				$this->retweet($result);

			}
		}
	}
}

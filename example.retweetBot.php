<?php

require_once('vendor/autoload.php');

// Email: 
// Login: 
// Password: 

$apiKey = "";
$apiSecret = "";
$accessToken = "";
$accessTokenSecret = "";

$t = new retweetBot($apiKey, $apiSecret, $accessToken, $accessTokenSecret);

// Set test most to true to stop actions from being posted to account
$t->testMode = true;

$t->tweetLimit(1);

$t->searchAndRetweet(array(
	"demo search",
));

echo "Done!\r\n";


